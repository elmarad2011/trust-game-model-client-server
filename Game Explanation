TRUST GAME MODEL 


1.	Introduction

The Trust Game Model simulates the behavior of a population of N Agents, divided into equal groups of Investor 
and Trustees. The core of the model is the Game module, in which each of the investors is given a certain quantity of
money with the option of either keeping their profit or transferring the quantity with a multiplying factor of b to 
a trustee (based on their strategy P0), whereas the trustee returns a fraction of the investment with an amount based
on his own Strategy r. One fundamental parameter for the execution of the transfer is the information Q, which 
describes the probability that the Investor has information about the trustees return policy before he initiates the 
transfer.
The model is executed in a number of N Cycles, with each cycle setting a different information value (0 < q < 1).
For each cycle, we run a number of Simulations where the agents are initialized with their role and their strategy
values. For each simulation a number of rounds are executed where in each round we play a number of N Games between
selected pair of Agents. After the game phase is finished and each of the agents has accumulated a certain payoff for
that round, the evolution phase in initiated where a pair of Investors and a pair of Trusties is selected randomly,
their payoffs  compared, and the agent with a lesser amount of payoff (based on a certain formula p) has his strategy
replaced with the strategy of the more profitable agent. After the rounds are executed, the simulation module stores
the Average strategies and payoffs of the Agents. 

2.	Module Hierarchy

In a top-to-bottom analysis of the code implementing the trust game model, the higher class is the Trust Game 
Interface, which provides a graphical interface for the user to input values for the parameters that define the 
model execution.

Figur 1

The Trust Game Full class file, implements the execution of the model, setting the different values of information 
for each simulation cycle. After the simulation cycle has ended, the file produces two different graphs (
Average strategies of Agents and Average strategies of Investors.) The user can also specify if he needs a third 
graph depicting specifically the variation of strategies and payoffs for a certain information value over the rounds 
of execution, by clicking the “ADD GRAPH FOR Q = “ checkbox in the interface and setting the corresponding value in 
the box.



for (int cycle_n = 0; cycle_n < Model_Constants.prec; cycle_n++)
		{
information = (double) cycle_n / (double) Model_Constants.prec; // information q gets values between 0 to 1, 
divided by the number of information precession,
	Model_Constants.q = information;
.
	Simulation sim =new Simulation(information);


Graph plotting example:
		
Graph_Plotter.title = "Average trust and return over information q, with b = " + Model_Constants.b;
Graph_Plotter.Xlabel = "Information q";
Graph_Plotter.Ylabel = "Average trust P0 and return r";
Graph_Plotter.Plot1 = "Average trust P0";
Graph_Plotter.Plot2 = "Average return r";
Graph_Plotter.chart = 0;
Graph_Plotter strategies =  new Graph_Plotter(Graph_Plotter.title);
strategies.pack();
RefineryUtilities.centerFrameOnScreen(strategies);


 
Figur 2


The Simulation file, initializes the Agents population and their corresponding strategies. The user, through the
interface has the option to bound the initial strategies of the investors for values of information ( 0 < q < 1/b). 

for (int i= 0; i < Model_Constants.INV_N; i++)	
//initialisation of Investors
		{
if ((Model_Constants.q >= 0) && (Model_Constants.q <= Trust_Game.b_ratio))
 // if information q is less than 1/b put a limit [0, q] on the initialisation values of r and PO
temp = generator.nextDouble() * Model_Constants.q;
else
	temp = generator.nextDouble();
	Investor inv = new Investor(temp);
	Investors.add(inv);

The Round  file implements the N number of games between the agents, which can either be selected randomly or having 
each of the investors playing with each trustee (set my the Game scenario variable):

if (Model_Constants.Game_Scenario == 0) // Randomly chosen inventor vs randomly chosen trustee
{
for (int i = 1; i <= Model_Constants.GAMES_N; i++)
			{
	tempi1 = generator1.nextInt(Model_Constants.INV_N);
	tempt1 = generator2.nextInt(Model_Constants.TR_N);
				
game = new Trust_Game(In.get(tempi1), Tr.get(tempt1));
}

if (Model_Constants.Game_Scenario == 1) // Each investor plays all trustees during each round
{
for (int j = 0; j < Model_Constants.INV_N; j++)
			{
	for (int k = 0; k < Model_Constants.TR_N; k++)							
	game = new Trust_Game(In.get(j), Tr.get(k));
				}
}

After the round is finished and the average payoff and strategy values of the agents are stored, the evolution phase 
is initiated where a pair of investors and a pair of trustees are selected randomly to be compared;

//executes the evolution phase at the end of the round
public void evolution(ArrayList<Investor> In, ArrayList<Trustee> Tr )
	{
for (int i=0; i< Model_Constants.evolution_rate; i++)
	{
tempi1 = generator1.nextInt(Model_Constants.INV_N);   // select the first inventor of the pair
	tempi2 = generator2.nextInt(Model_Constants.INV_N);	 // select the second inventor of the pair
	if ((In.get(tempi1).Average_Payoffs.size() > 0) && (In.get(tempi2).Average_Payoffs.size() > 0))   // The pair
    has stored payoffs from this or previous rounds
	{
			
evolveInv(In.get(tempi1), In.get(tempi2));  // evolve the pair

// recieves Investor pair and executes the evolution model for their strategies
	public boolean evolveInv(Investor A, Investor B)
	{
	temp = 0;
	//Store the average payoff of the agent
	size = A.Average_Payoffs.size();
	PA = A.Average_Payoffs.get(size -1);
	    //store the stationary strategy of the agent
	size = A.Evolution_Str.size();
	stationary80 = ((size * 80) / 100) ;  // variable used to calculate the 80 % last of all the average strategies 
    stored in the agent

if (size >= 10)
	{
	for (int i = size -1; i > (size - stationary80 -1); i --)
	{
		temp = temp + A.Evolution_Str.get(i);
	}
	Stat_P01 = (double)  (temp /  stationary80) ;		// calculates average strategy for Agent A
	}
	else
	if ((size > 0) && (size < 10))
	Stat_P01 = A.Evolution_Str.get(size -1);
	if (size == 0)
	  Stat_P01 = A.Strategy;
	}
	temp = 0;
	//Store the average payoff of the agent
	size = B.Average_Payoffs.size();
		
p = calculateP (PA, PB);			// calculates p value based on PA, PB values

	temp = generator3.nextDouble();
	if (temp <= (p * (1 - Model_Constants.mi)))   // propabillity P * (1-m)
{
	B.Evolution_Str.add(B.Strategy);	   // Add current strategy of B into Strategy Pool
B.Strategy = Stat_P01;					  // B strategy replaced with stationary A
	B.Evolution_Str.add(Stat_P01); 	// Add new strategy value to Pool
	}
	else if (temp <= Model_Constants.mi)		  // propability mi
	{
	B.Strategy = generator3.nextDouble();      // B Strategy replaced with random mutant value

		return true;
	}



The Trust Game file executes the game between the selected pair of Agents. Firstly we check if we should initiate 
the transfer between the agents:

public boolean Inv_Strategy_Check(Investor I, Trustee T)
	{
		//System.out.println("Checking strategies...");
		Random rand1 = new Random();
		Random rand2 = new Random();		
		double tempq, tempP;
		boolean transfer = false;
		tempq = rand1.nextDouble(); // random double in the range 0 - 1
		tempP = rand2.nextDouble(); // random double in the range 0 - 1
		if ((tempq <= q) && (T.Strategy >= b_ratio)) // transfer with information prop. q in case r larger or equal 
        to 1/b quantity
		{
		transfer = true;
		}
		if ((tempq <= (1 - q)) && (tempP <= I.Strategy) && (transfer == false))  // transfer with 1-q prop. in and 
		Investor strategy prop. P0
		{
		transfer = true;
		}
		return transfer;
	}

In case the boolean variable transfer returns true and the transfer is to be executed, we perform the quantity 
transfer to the payoff of each agent:

 	// implements the money transfer from the part of Investor
	public boolean Inv_Transfer(Investor I,Trustee T)
	{
		boolean tr_done = false;
		T.Payoff = T.Payoff + ((1-T.Strategy)*Model_Constants.b);
		tr_done = true;
		return tr_done;
	}
	
	// implements the money transfer from the part of Trustee
	public boolean Tr_Transfer(Investor I, Trustee T)
	{
		boolean tr_done = false;
		I.Payoff = I.Payoff + (T.Strategy*Model_Constants.b);
		tr_done = true;
		return tr_done;
	}	


Two peripheral code files, the Model_Constants and the Log file keep track of the static model parameters and 
the log file word document output which captures all the details of the model execution. The user can specify the 
“visibility” of the log file to be produced by checking the corresponding boxes in the interface.
Lastly, the Graph_Plotter class, produces the graphs of the model, based on the average values of the agents that 
have been produced during the execution of the model.

3.	Appendix and Extensions

•	Reputation Scheme

The reputation scheme enables investors to vote trustees according to their return strategies. Other investors are 
able to know the reputation of a certain trustee in order to decide if the will enact in a transfer game in between 
them.
From a coding perspective, we differentiate between 2 different voting scenarios:
•	Knowledge of return strategy from information value q: In this scenario, an investor is able to know the return 
strategy of a trustee before the transfer, thus he can “vote” him depending on his r ratio:

		// investor rates the trustee
T.reputation = T.reputation + ((0.9) * T.Strategy) + ((0.1) * (I.predesposition)) ;	

•	Return strategy of Trustee not known: In this scenario, an investor makes a transfer to a trustee, acquires his 
return strategy and then “votes” him:

		// investor rates the trustee
T.reputation = T.reputation + ((0.75) * T.Strategy) + ((0.25) * (I.predesposition)) ;		
			}	
One critical difference in between the 2 different executions of the voting scheme is the influence of an investor’s 
own “predisposition” towards trustees, that is a value of 0 -> 1 depending on how “reluctant or favorable” of a 
transfer an investor is without any knowledge of an existing trustee. In the first case the influence of this 
parameter is limited to a 10% (0.1 factor) as the investor has real information about the trustee and his own 
psychology is not affecting his rational thinking. In the second case, as information about the trustee is yet to be 
determined, his own tendency is influencing the result to a greater extent: 25% (0.25) factor. 

•	Ultimatum Game

The Ultimatum game, occurs in the trust game model, when the information value of investors reaches the point q = 1. 
In this phase, all return strategies of trustees are known and investors are forced to invest their quantities 
without choosing beforehand to keep them:
		// check for ultimatum game execution
if ((Model_Constants.ultimatum) && (Model_Constants.q == 1))
		ult = true;
else
		ult = false;

